---
title: RCF-RCP Interface Specification
author:
- Jack Hickish\textsuperscript{1}
affiliation:
- \textsuperscript{1} Real-Time Radio Systems Ltd
abstract:
document-number: 00019
wbs-level-2-abbrev: RCF
document-type-abbrev: ICD
revisions:
- version: 1
  date: 2023-08-01
  remarks: Original
  authors:
  - JH
- version: 2
  date: 2023-08-14
  sections:
  - sec:InterfaceDefinition
  remarks: Change `N_CHAN` requirement to be a multiple of 4 (was 8). Clarify that `N_TIME` is the total number of times in a packet including the ``fine time'' axis.
  authors:
  - JH
- version: 3
  date: 2023-10-17
  remarks: Re-title to use DSA document numbering. Port to DSA document template.
  authors:
  - JH
- version: 4
  date: 2023-10-19
  remarks: Title change only
  authors:
  - JH
- version: 4.0.1
  date: 2024-14-05
  remarks: Template update only
  authors:
  - JH
- version: 4.1.0
  date: 2024-02-10
  remarks: Flip real/imag ordering to match correlator's native input format.
  authors:
  - JH
- version: 5.0.0
  date: 2025-03-06
  remarks: Add packet_time header field
  authors:
  - JH
...
