#! /bin/bash
pdfname=D2k-00019-RCF-ICD-RCF_RCP_interface.pdf

pandoc docs/*.md -o ${pdfname} -s -V colorlinks -V links-as-notes --number-sections --template doc-templates/pandoc/template.latex --citeproc
